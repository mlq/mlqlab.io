const isProd = process.env.NODE_ENV === 'production'

/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'export',
  reactStrictMode: true,
  images: {
    unoptimized: true,
  },
  assetPrefix: isProd ? 'https://mlq.gitlab.io/' : '',
}

module.exports = nextConfig
