import { ProgressiveImage, H2 } from "@/uikit";
import { portfolio } from './portfolio/portfolio';
import Link from "next/link";

export default function Home() {

  return (
    <main className='container'>

      <div className="row pb-5">

        <H2>Products</H2>

        { portfolio.map((portfolioItem, i) => (
          <div className="col-lg-8" key={i}>
            <Link href={portfolioItem.link}>
              <ProgressiveImage
                preview={portfolioItem.preview}
                image={portfolioItem.src}
                alt={portfolioItem.alt}
                interactive={portfolioItem.interactive}
                description={portfolioItem.description}
              />
            </Link>
          </div>
        ))}


      </div>

    </main>
  )
}
