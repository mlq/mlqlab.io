'use client'

import s from "./Navigation.module.scss";
import Link from "next/link";
import { usePathname } from 'next/navigation'
import colors from '../../styles/colors-semantic.module.scss';

const navLinks = [
  { title: 'Home', href: '/' },
  { title: 'Contacts', href: '/contacts' },
  { title: 'About', href: '/about'}
]

export default function Navigation() {

  const pathname = usePathname();

  return (
    <div className={s.Menu}>
      { navLinks.map((link, i) => (
        <div key={i}>
          <Link href={link.href}>
            <span style={{ color: `${pathname === link.href ? colors['colorTextLink'] : 'inherit'}` }}>
              {link.title}
            </span>
          </Link>
        </div>
      ))}
    </div>
  )
}
