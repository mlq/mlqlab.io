'use client'

import React, { useEffect, useState } from "react";
import {Gallery} from "@/uikit";
import { useRouter } from 'next/navigation'
import { portfolio } from '../../portfolio/portfolio';

export default function Page({ params }: { params: { galleryName: string } }) {

  const router = useRouter();

  const [ imageList, setImageList] = useState<string[]>();


  useEffect(() => {
    portfolio.map((portfolioItem) => {
      if (portfolioItem.name === params.galleryName) {
        setImageList(portfolioItem.gallery || [] as string[]);
      }
    });
  }, [ params.galleryName ])


  return (
    <div>
      <Gallery images={imageList || []} closeHandler={() => router.push('/')} >
        My Post: {params.galleryName}
        <div className="container">

        </div>
      </Gallery>
    </div>
  );
}
