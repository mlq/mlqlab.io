import React from 'react';
import s from './layout.module.scss';

export default function GalleryLayout({ children }: {
  children: React.ReactNode
}) {

  return (
    <div className={s.GalleryPage}>
      {children}
    </div>
  )
}
