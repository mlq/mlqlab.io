import React from 'react';
import s from './Gallery.module.scss';
import { RiCloseFill } from 'react-icons/ri';
import Image from 'next/image';
import { ProgressiveImage } from "@/uikit";

interface IGalleryProps {
  children: React.ReactNode;
  closeHandler: () => void;
  images: string[];
}

export const Gallery: React.FC<IGalleryProps> = ({ children, closeHandler, images }) => {

  const onClose = () => closeHandler && closeHandler();

  return (
    <div className={s.Gallery}>

      <div className={s.GalleryBackdrop} onClick={onClose}/>

      <div className={s.GalleryClose}>
        <RiCloseFill onClick={onClose} />
      </div>

      <div className={s.GalleryContent}>
        <div className={'p-3'}>
          {children}
        </div>
        {images.map((image, i) => (
          <ProgressiveImage
            key={i}
            preview={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAUCAYAAADlep81AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAQUSURBVHgBzZbNT1xVGMZ/537MJzNQGGDko7QFRLQaK7F2gXFh0taYRlcmxo0rE0x05x/Af+DemC6aNHWh0WgTbWJNA2MyxKCpFWgpdWboAANMmGGY77l3POdCG2McBtBF3+Tm3nvOuec853ne87xXJJPJOk9OTGo8QWHbNgZHjGg0SiaToVwuY1kWQ0NDzrsQgvHxcY4a4r9IVigUHBButxvTNKnVarS3t3PUkAxNHpkhFT6fz7n+zzDq9cMRFIlEiExHUF8J8ehbgdMgM3J3PqFaZFN97wkmJiYIBAJN5z80oJmZGd798BM0vU6bz8Tn9eDSBJVKmUx+m6Bh02aCaejs5HckJIuvv72BTA1GRkb2nftISa32294ZxGsIfKaGvElibPwuA1E3yW2m0b0G1VoJr1uTjFVYWl1jcPRg8x/62Cs+A24dUS2RXFoiOjXF1vom8fU0D9KSEaFx++4DdNtiPr7Blen7/HR7hYMqoamBh7lU5HJFvvshQmJ5g1bPMe5PfY/XbCUgCc9UXARCA8yvWCykKmxlLMw9IQ4yd1PJ1MBqtfr4vb+/n3ohT0vAT1s4zMpWlpmHdfKffsZHH39AqybHWzbb/gLBUIjh4xVyD2McNEQikdiXS+UtFeHB43ZJcAK7bpMr2XQE3STSJdazRQLJOabn7xGyU8yl13nj0vsMDg5x7fJlNlNpLJmsNbmxV84+z8WL59ln8819SMicSG1m8fu81ITJ78kSp7o8dLf7GexuoTvgImv28WZniF9jy3TWEsxFZ3n65AlePjNGuKMPYbpk2guWYrPNlmss2U6xRrZiEDBqXIvEGegK8MKpMKOdgnypwNWvovT1PMXpkZNcv/4NkmmnhJRKFXTT4M7kb4w99yxnLnRTqRtsZ9bIpddoSkAsFvtXye4mUiz8ucboQDdC99IdaiMYbOGPpWU+v/IlPT1hWlo7eOv1s+jlTTweD4uLiwwPDzt+oqS+8cVVKqtJ3ntnjJ9/nKU8dI7zl95uCGZfyQKiSDm1QLnNInx8lKJM5K7QMaxinguvvoTf7+P06DO0B32sJjeIx+O4XC5WV1cfG2BZelE8dodfbhbwubzUzea213CE285zbriDVHaNWzdjMpekS8tc0A3DYSAtj/Ot5D002WHKhXp7e+nr68OQ/YoddW8L97B5YpTotk5hp8hrLzYvvA0l02U5cMsdF0ol+YtRdaqS5pSlR1WKvSqlLN/C6/U6IJRNKEAOQ/LXBEvmlK6jbKaumZJFsyEYR7JGDipJcMBomibl8Tura/LE1aTHKIbU6dM1nP8fBUiFSmr1rgDsbkoVFRcuU0HXqNo0deyGktl7H1oOgOrfeoTTpiS0JShV8dUiCsg/disLsL4rsepTY7Tm5eMvQeMNcfDDc+gAAAAASUVORK5CYII="}
            image={image}
            alt={'Temp'}
          />
        ))}
      </div>

    </div>
  );
}
